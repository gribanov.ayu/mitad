from math import sqrt
import random


class kNN:
    def __init__(self):
        self.__data, self.classes = self.__integrify_classes(self.__load_data())

    @staticmethod
    def __euclidean_distance(row1, row2):
        return sqrt(sum((row1[i] - row2[i]) ** 2 for i in range(len(row1))))

    @staticmethod
    def __manhattan_distance(row1, row2, n=2):
        distance = sum(abs(row1[i] - row2[i]) ** n for i in range(len(row1)))
        return distance ** (1 / n)

    @staticmethod
    def __sort(lst):
        lst.sort(key=lambda x: x[1])
        return lst

    def __load_data(self):
        with open('iris.csv', 'r', encoding='utf8') as f:
            data = []
            for i in f.readlines():
                arr = []
                st = i.strip().split(',')

                for j in range(0, len(st) - 1):
                    arr.append(float(st[j]))
                arr.append(st[-1])

                data.append(arr)
            return data

    def __integrify_classes(self, data):
        res = list(set([i[-1] for i in data]))
        for i in data:
            if i[-1] in res:
                i[-1] = res.index(i[-1])

        return data, res

    def get_random(self):
        return random.choice(self.__data)

    def __get_neighbors(self, looking, n, f):
        tmp = \
            self.__sort([(i, self.__euclidean_distance(looking, i)) for i in self.__data]) if f == 'e' \
            else self.__sort([(i, self.__manhattan_distance(looking, i)) for i in self.__data])

        return [i[0] for i in tmp][:n]

    def get_prediction(self, looking, n, f='e'):
        result = [row[-1] for row in self.__get_neighbors(looking, n, f)]
        predicted_index = max(set(result))
        return self.classes[predicted_index]


if __name__ == '__main__':
    knn = kNN()
    n_nei = 3
    rp = knn.get_random()
    cl = knn.classes

    print('Predicting:\t{} - > {}'.format(cl[rp[-1]], ', '.join(str(i) for i in rp[:-1])))

    res = knn.get_prediction(rp[:-1], n_nei, 'e')
    print('Received:\t' + res)
