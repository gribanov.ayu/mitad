# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\knn.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox

from main import kNN


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(380, 220)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.pushButton = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton.setGeometry(QtCore.QRect(90, 70, 80, 21))
        self.pushButton.setObjectName("pushButton")
        self.Euclidean = QtWidgets.QRadioButton(self.centralWidget)
        self.Euclidean.setEnabled(True)
        self.Euclidean.setGeometry(QtCore.QRect(90, 20, 84, 19))
        self.Euclidean.setChecked(True)
        self.Euclidean.setObjectName("Euclidean")
        self.Manhattan = QtWidgets.QRadioButton(self.centralWidget)
        self.Manhattan.setGeometry(QtCore.QRect(190, 20, 84, 19))
        self.Manhattan.setObjectName("Manhattan")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton_2.setGeometry(QtCore.QRect(190, 70, 80, 21))
        self.pushButton_2.setObjectName("pushButton_2")
        self.label = QtWidgets.QLabel(self.centralWidget)
        self.label.setGeometry(QtCore.QRect(20, 110, 81, 21))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralWidget)
        self.label_2.setGeometry(QtCore.QRect(20, 150, 71, 16))
        self.label_2.setObjectName("label_2")
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralWidget)
        self.lineEdit_2.setGeometry(QtCore.QRect(110, 110, 251, 21))
        self.lineEdit_2.setObjectName("lineEdit")
        self.lineEdit = QtWidgets.QLineEdit(self.centralWidget)
        self.lineEdit.setGeometry(QtCore.QRect(50, 70, 21, 21))
        self.lineEdit.setObjectName("lineEdit_2")
        self.lineEdit_3 = QtWidgets.QLineEdit(self.centralWidget)
        self.lineEdit_3.setGeometry(QtCore.QRect(110, 150, 251, 21))
        self.lineEdit_3.setObjectName("lineEdit_3")

        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 380, 20))
        self.menuBar.setObjectName("menuBar")
        MainWindow.setMenuBar(self.menuBar)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")

        MainWindow.setFixedSize(380, 220)
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)

        self.kNN = kNN()
        init_class = self.kNN.get_random()
        self.lineEdit_2.setText(', '.join(str(x) for x in init_class[:-1]) + ', ' + self.kNN.classes[init_class[-1]])

        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "KNN"))
        self.pushButton.setText(_translate("MainWindow", "Predict"))
        self.Euclidean.setText(_translate("MainWindow", "Euclidean"))
        self.Manhattan.setText(_translate("MainWindow", "Мanhattan"))
        self.pushButton_2.setText(_translate("MainWindow", "Get random"))
        self.label.setText(_translate("MainWindow", "Expected:"))
        self.label_2.setText(_translate("MainWindow", "Received:"))

        self.lineEdit.setText('3')

        self.pushButton.clicked.connect(self.predictButton)
        self.pushButton_2.clicked.connect(self.randomButton)

    def predictButton(self):
        func = 'e' if self.Euclidean.isChecked() else 'm'

        if self.lineEdit.text().isnumeric():
            num_n = int(self.lineEdit.text())
        else:
            self.__errorHandler(0)
            return

        test = [float(x) for x in self.lineEdit_2.text().split(',')[:-1]]

        if len(test) == 4:
            res = self.kNN.get_prediction(test, num_n, func)
            self.lineEdit_3.setText(res)
        else:
            self.__errorHandler(1)
            return

    def __errorHandler(self, reason):
        message = "Please put numeric values." if reason == 0 else "Please put correct values."
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText(message)
        msg.setWindowTitle("Error!")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def randomButton(self):
        self.lineEdit_2.clear()
        self.lineEdit_3.clear()

        rand_value = self.kNN.get_random()

        self.lineEdit_2.setText(', '.join(str(x) for x in rand_value[:-1]) + ', ' + self.kNN.classes[rand_value[-1]])


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
