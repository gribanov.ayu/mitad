---
title: "Практическая работа №4. Анализ данных в среде RStudio."
date: "27 05 2020"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## 1) Подготовка данных

В качестве исходного набора данных используем набор данных об отсутствии или наличие сердечной аритмии. Набор данных содержит 279 атрибутов, 206 из которых имеют линейное значение, а остальные являются номинальными.

Цель состоит в том, чтобы различать наличие и отсутствие сердечной аритмии и классифицировать ее в одной из 16 групп.

Сам набор можно найти по ссылке: http://archive.ics.uci.edu/ml/datasets/Arrhythmia

Ссылка на исследование: http://www.cs.bilkent.edu.tr/tech-reports/1998/BU-CEIS-9802.pdf

```{r, echo=FALSE}
path <- 'C:\\Users\\anon\\Desktop\\lab4_2\\arrhythmia.data'
```
```{r}
data <- read.table(path, sep = ",", header = FALSE)
colnames(data) <- c('age', 'sex', 'height', 'weight', 'qrs_duration', 'p_r_interval', 'q_t_interval', 't_interval', 'p_interval', 'qrs', 'T', 'P', 'QRST', 'J', 'heart_rate', 'q_wave', 'r_wave', 's_wave', 'R_wave', 'S_wave', 'AA', 'AB1', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AR', 'AS', 'AT', 'AU', 'AV', 'AY', 'AZ', 'AB2', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BR', 'BS', 'BT', 'BU', 'BV', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'Cf', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CR', 'CS', 'CT', 'CU', 'CV', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DR', 'DS', 'DT', 'DU', 'DV', 'DY', 'DZ', 'EA', 'EB', 'EC', 'ED', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'ER', 'ES', 'ET', 'EU', 'EV', 'EY', 'EZ', 'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FR', 'FS', 'FT', 'FU', 'FV', 'FY', 'FZ', 'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GR', 'GS', 'GT', 'GU', 'GV', 'GY', 'GZ', 'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HR', 'HS', 'HT', 'HU', 'HV', 'HY', 'HZ', 'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IR', 'IS', 'IT', 'IU', 'IV', 'IY', 'IZ', 'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JR', 'JS', 'JT', 'JU', 'JV', 'JY', 'JZ', 'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KR', 'KS', 'KT', 'KU', 'KV', 'KY', 'KZ', 'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'diagnosis')

dim(data)
```

## 2) Cтатистический анализ по показателям

Суммарные показатели по возрасту:
```{r}
summary(data$age)
```

Суммарные показатели по весу:
```{r}
summary(data$weight)
```

Суммарные показатели по полу:
```{r}
summary(data$sex)
```

Суммарные показатели по росту:
```{r}
summary(data$height)
```

Суммарные показатели по QRS:
```{r}
summary(data$qrs_duration)
```

Визуализация данных по возрасту/весу:
```{r}
plot(data$age ~ data$weight, col = rainbow(10), ylab="Возраст", xlab="Вес")
```

Визуализация по весу/диагнозу:
```{r}
library(lattice)
xyplot(data$weight ~ data$diagnosis, col = rainbow(10), ylab="Вес", xlab="Диагноз")
```

Визуализация по сердцебиению/диагнозу:
```{r}
barplot(table(data$heart_rate, data$diagnosis), col = "red", ylab="Ритм", xlab="Диагноз")
```

Согласно исследованию атрибут *diagnosis* отражает есть ли аритмия или нет, где 1 означает что аритмии нет, в то время как остальные означают отклонения. Преобразуем набор данных для простоты дальнейшей работы организовав все откланения в один класс.

```{r, echo=FALSE}
path <- 'C:\\Users\\anon\\Desktop\\lab4_2\\updated.data'
data <- read.table(path, sep = ",", header = FALSE)
colnames(data) <- c('age', 'sex', 'height', 'weight', 'qrs_duration', 'p_r_interval', 'q_t_interval', 't_interval', 'p_interval', 'qrs', 'T', 'P', 'QRST', 'J', 'heart_rate', 'q_wave', 'r_wave', 's_wave', 'R_wave', 'S_wave', 'AA', 'AB1', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AR', 'AS', 'AT', 'AU', 'AV', 'AY', 'AZ', 'AB2', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BR', 'BS', 'BT', 'BU', 'BV', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'Cf', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CR', 'CS', 'CT', 'CU', 'CV', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DR', 'DS', 'DT', 'DU', 'DV', 'DY', 'DZ', 'EA', 'EB', 'EC', 'ED', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'ER', 'ES', 'ET', 'EU', 'EV', 'EY', 'EZ', 'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FR', 'FS', 'FT', 'FU', 'FV', 'FY', 'FZ', 'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GR', 'GS', 'GT', 'GU', 'GV', 'GY', 'GZ', 'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HR', 'HS', 'HT', 'HU', 'HV', 'HY', 'HZ', 'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IR', 'IS', 'IT', 'IU', 'IV', 'IY', 'IZ', 'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JR', 'JS', 'JT', 'JU', 'JV', 'JY', 'JZ', 'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KR', 'KS', 'KT', 'KU', 'KV', 'KY', 'KZ', 'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'diagnosis')
```

## 3) Cтатистический анализ зависимости класса от отдельных показателей

Диаграмма размаха по возрасту/диагнозу:
```{r}
boxplot(data$age ~ data$diagnosis, col = rainbow(10), ylab="Возраст", xlab="Диагноз")
```

Диаграмма размаха по весу/диагнозу:
```{r}
boxplot(data$weight ~ data$diagnosis, col = rainbow(10), ylab="Вес", xlab="Диагноз")
```

Результаты wilcox.test:
```{r}
# По весу
wilcox.test(data$weight, data$diagnosis)
```

```{r}
# По полу
wilcox.test(data$sex, data$diagnosis)
```

Визуализация по половому признаку:
```{r}
barplot(table(data$sex, data$diagnosis), col = rainbow(2), ylab="Пол", xlab="Диагноз", legend.text = c("М", "Ж"))
```

Похоже женщины меньше страдают от арритмии.

Результаты kruskal.test:
```{r}
# По возрасту
kruskal.test(data$age ~ data$diagnosis)
```

Визуализация по возростному признаку:
```{r}
# По возрасту
boxplot(data$age ~ data$diagnosis, col = rainbow(2), ylab="Возраст", xlab="Диагноз")
```

Как видно арритмией страдают больше люди пожилого возраста.

## 4.1) Классификация данных с помощью методов knn

```{r}
library(class)

train.size <- floor(nrow(data) * .8)
train.index <- sample(seq_len(nrow(data)), size = train.size)
data.train <- data[train.index,]
data.test <- data[-train.index,]

barplot(table(data.train$diagnosis), col = rainbow(2))
```

```{r}
barplot(table(data.test$diagnosis), col = rainbow(2))
```

Точность и матрица ошибок при k = 3:

```{r}
classIndex <- 280

predicted <- knn(train = data.train[, -classIndex], cl = data.train[, classIndex], test = data.test[, -classIndex], k = 3)

# Точность
sum(predicted == data.test[, classIndex]) / nrow(data.test)
```
```{r}
table(PREDICTED = predicted, REAL = data.test[, classIndex])
```
```{r}
mosaicplot(table(PREDICTED = predicted, REAL = data.test[, classIndex]), main = "knn = 3", col = rainbow(2))
```

Точность и матрица ошибок при k = 15:

```{r}
predicted <- knn(train = data.train[, -classIndex], cl = data.train[, classIndex], test = data.test[, -classIndex], k = 15)

# Точность
sum(predicted == data.test[, classIndex]) / nrow(data.test)
```
```{r}
table(PREDICTED = predicted, REAL = data.test[, classIndex])
```

```{r}
mosaicplot(table(PREDICTED = predicted, REAL = data.test[, classIndex]), main = "knn = 15", col = rainbow(2))
``` 

## 4.2) Дерево решений

```{r}
library(tree)

tmp <- read.table(path, sep = ",", header = FALSE)

tmp <- cbind(data[, -classIndex], factor(data$diagnosis))
tree <- tree(data[, classIndex] ~., data[, -classIndex])
plot(tree)
text(tree)
```

## 4.3) Случайный лес

```{r}
library(randomForest)

tmp <- data
tmp$diagnosis <- factor(tmp$diagnosis)
rand_forest <- randomForest(diagnosis ~., tmp)
rand_forest
```

## 4.4) Байесовская классификация

```{r}
library(h2o)

h2o.init()

tmp <- data
tmp$diagnosis <- factor(tmp$diagnosis)

train_nb.size <- floor(nrow(tmp) * .8)
train_nb.index <- sample(seq_len(nrow(tmp)), size = train_nb.size)
train_nb.train <- tmp[train_nb.index,]
train_nb.test <- tmp[-train_nb.index,]
nb_train <- as.h2o(train_nb.train)
nb_test <- as.h2o(train_nb.test)

h2o.naiveBayes(, 280, training_frame = nb_train, validation_frame = nb_test)
```

## 4.5) Нейронная сеть

```{r}
library(neuralnet)
library(nnet)

tmp <- data

train_nn.size <- floor(nrow(tmp) * .8)
train_nn.ind <- sample(seq_len(nrow(tmp)), size = train_nn.size)
train_nn.train <- tmp[train_nn.ind,]
train_nn.test <- tmp[-train_nn.ind,]

class.params <- class.ind(tmp$diagnosis)
colnames(class.params) <- paste("D", 0:1, sep = "")
nn.train <- cbind(class.params[train_nn.ind,], train_nn.train)

lpart <- paste(colnames(class.params), collapse = " + ")
numb.formula <- paste(lpart, "~.")

numb.nn <- neuralnet(numb.formula, nn.train, hidden = c(25, 12, 5), linear.output = FALSE)

plot(numb.nn)
```

```{r}
numb.nn$result.matrix[1:5,]
```

## 5) Анализ важности показателей с помощью пакета randomForest

```{r}
result <- importance(rand_forest)
head(result)
```

Самые важные показатели:
**ДОБАВИТЬ "СНИМОК"**

```{r}
barplot(table(data$diagnosis, data$heart_rate), col = rainbow(2))
```

```{r}
barplot(table(data$diagnosis, data$qrs_duration), col = rainbow(2))
```

```{r}
barplot(table(data$diagnosis, data$LE), col = rainbow(2))
```

## 6) Классификация данных на подмножестве «важных» показателей

Выведем самыме важные показателями в отдельный набор данных:

```{r}
f_data <- data
f_data[1:279] <- NULL

f_data <- cbind(data$heart_rate, data$qrs_duration, data$LE, data$DB, data$IV, data$DD, data$HR, data$JB, data$GJ, data$GY, data$GL, data$LG, data$HT, data$JG, data$KS, data$KK, data$GU, data$JZ, data$ID, data$t_interval, data$KJ,  data$q_t_interval, data$CJ, data$KA, data$JN, f_data)
names(f_data) <- c("heart_rate", "qrs_duration", "LE", "DB", "IV", "DD", "HR", "JB", "GJ", "GY", "GL", "LG", "HT", "JG", "KS", "KK", "GU", "JZ", "ID", "t_interval", "KJ", "q_t_interval", "CJ", "KA", "JN", "Target")
head(f_data)
```

```{r}
f_data.size <- floor(nrow(data) * .8)
f_data.index <- sample(seq_len(nrow(f_data)), size = train.size)
f_data.train <- f_data[train.index,]
f_data.test <- f_data[-train.index,]

indClass <- 26

predicted <- knn(train = f_data.train, cl = f_data.train[, indClass], test = f_data.test, k = 3)

table(predicted, f_data.test$Target)
```

```{r}
plot(table(predicted, f_data.test$Target), col = rainbow(2), main = '')
```

Точность классификации:
```{r}
sum(predicted == f_data.test$Target) / sum(predicted == f_data.test$Target | predicted != f_data.test$Target)
```

Построение дерева решений:
```{r}
tree <- tree(f_data$Target ~., f_data[,-indClass])
plot(tree)
text(tree)
```

Работа с Байесовской классификацией:
```{r}
h2o.init()

tmp <- f_data
tmp$Target <- factor(tmp$Target)

train_nb.size <- floor(nrow(tmp) * .8)
train_nb.index <- sample(seq_len(nrow(tmp)), size = train_nb.size)
train_nb.train <- tmp[train_nb.index,]
train_nb.test <- tmp[-train_nb.index,]
nb_train <- as.h2o(train_nb.train)
nb_test <- as.h2o(train_nb.test)

h2o.naiveBayes(, indClass, training_frame = nb_train, validation_frame = nb_test)
```

Работа с нейронной сетью:

```{r}
tmp <- f_data

train_nn.size <- floor(nrow(tmp) * .8)
train_nn.index <- sample(seq_len(nrow(tmp)), size = train_nb.size)
train_nn.train <- tmp[train_nb.index,]
train_nn.test <- tmp[-train_nb.index,]

train_nn.train$D0 <- c(train_nn.train$Target == 0)
train_nn.train$D1 <- c(train_nn.train$Target == 1)
train_nn.train$Target <- NULL

train_nn.test$D0 <- c(train_nn.test$Target == 0)
train_nn.test$D1 <- c(train_nn.test$Target == 1)
train_nn.test$Target <- NULL

formula <- as.formula(paste(paste(paste("D", 0:1, sep = ""), collapse = " + "), " ~ ."))

nn <- neuralnet(formula, data = train_nn.train, hidden = c(15, 10, 5), linear.output = FALSE)
plot(nn)
```

```{r}
head(nn$result.matrix[1:3,])
```

Проверка нейросети на случайных данных:
```{r}
# 1)
predict <- compute(nn, train_nn.test[1:25])
output <- predict$net.result[3,]
round(output, digits = 0)
```

```{r}
train_nn.test[3, c(26, 27)]
```

```{r}
# 2)
predict <- compute(nn, train_nn.test[1:25])
output <- predict$net.result[5,]
round(output, digits = 0)
```

```{r}
train_nn.test[5, c(26, 27)]
```

```{r}
# 3)
predict <- compute(nn, train_nn.test[1:25])
output <- predict$net.result[15,]
round(output, digits = 0)
```

```{r}
train_nn.test[15, c(26, 27)]
```
