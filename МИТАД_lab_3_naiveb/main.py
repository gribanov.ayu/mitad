# Алгоритм Байесовской классификации
# На примере iris dataset
import requests
import random
from math import sqrt, pi, exp


class NaiveBayesIris(object):
    def __init__(self):
        self.data, self.classes = self.__get_iris()
        self.model = None

        self.to_predict = None
        self.expected_class = None

        self.result = None

    # 1) Подготовка данных
    @staticmethod
    def __get_iris():
        st = str(
            requests.get('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data'
                         ).content.strip())

        if st[:2] == 'b\'':
            st = st[2:]

        if st[-1] == '\'':
            st = st[:-1]

        tmp = [i.split(',') for i in st.split('\\n')]

        for i in range(len(tmp[0]) - 1):
            for row in tmp:
                row[i] = float(row[i].strip())

        class_values = set([row[len(tmp[0]) - 1] for row in tmp])
        classes = dict()
        for i, value in enumerate(class_values):
            classes[value] = i

        for i in tmp:
            i[len(tmp[0]) - 1] = classes[i[len(tmp[0]) - 1]]

        return tmp, classes

    # 2) Создание модели, организация данных по классу
    # Среднее значение
    @staticmethod
    def __mean(n):
        return sum(n) / float(len(n))

    # Среднеквадратическое отклонение
    @staticmethod
    def __stdev(n):
        return sqrt(sum([(x - NaiveBayesIris.__mean(n)) ** 2 for x in n]) / float(len(n) - 1))

    # Рассчитать среднее значение, среднеквадратическое отклонение и количество для каждого столбца в наборе данных
    def __calc_data(self, rows):
        summaries = [(self.__mean(column), self.__stdev(column), len(column)) for column in zip(*rows)][:-1]
        return summaries

    # Разделить набор данных по классам, а затем рассчитать статистику для каждой строки
    def create_model(self):
        class_split = dict()
        for i in range(len(self.data)):
            vector = self.data[i]
            class_value = vector[-1]
            if class_value not in class_split:
                class_split[class_value] = list()
            class_split[class_value].append(vector)

        self.model = dict()
        for class_value, rows in class_split.items():
            self.model[class_value] = self.__calc_data(rows)

    # 3) Достать случайные данные и класс для проверки
    def get_random_iris(self):
        item = random.choice(self.data)

        for name, i in self.classes.items():  # for name, age in dictionary.iteritems():  (for Python 2.x)
            if i == item[-1]:
                self.to_predict, self.expected_class = item[:-1], name
                break

    # 4) Предсказать класс по случайным данным
    # Гауссовская функцию распределения вероятностей для х
    @staticmethod
    def __GPD(x, mean, stdev):
        exponent = exp(-((x - mean) ** 2 / (2 * stdev ** 2)))
        return (1 / (sqrt(2 * pi) * stdev)) * exponent

    # Рассчет вероятности прогнозирования каждого класса для данной строки
    def __calc_class_probabilities(self):
        total_rows = sum([self.model[i][0][2] for i in self.model])

        probabilities = dict()

        for class_value, class_summaries in self.model.items():
            probabilities[class_value] = self.model[class_value][0][2] / float(total_rows)
            for i in range(len(class_summaries)):
                mean, stdev, _ = class_summaries[i]
                probabilities[class_value] *= self.__GPD(self.to_predict[i], mean, stdev)

        return probabilities

    # Предсказать класс по данным
    def predict(self):
        probabilities = self.__calc_class_probabilities()
        best_prob = -1
        for class_value, probability in probabilities.items():
            if self.result is None or probability > best_prob:
                best_prob = probability
                self.result = class_value

    # 5) Вывод результата
    def output_result(self):
        for name, i in self.classes.items():  # for name, age in dictionary.iteritems():  (for Python 2.x)
            if i == self.result:
                print('Data={}\nPredicted: {}\nExpected: {}'.format(self.to_predict, name, self.expected_class))


if __name__ == '__main__':
    nb = NaiveBayesIris()
    nb.create_model()
    nb.get_random_iris()
    nb.predict()
    nb.output_result()
